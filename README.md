nginx
=====

Simple nginx install with a sample nginx.conf template:
- port 80 reverse proxy to port 3000
- /assets location handles static assets with cors
- Override with application specific config file

Requirements
------------

No known pre-requisites.

Role Variables
--------------

**Explicitly override template with application specific nginx configuration**  
`nginx_conf: /User/bencooling/projects/personal/app/ansible/nginx.conf`

Dependencies
------------

No dependencies.

Example Playbook
----------------

```
- hosts: servers
  roles:
     - { role: bencooling.nginx, nginx_conf: /User/bencooling/projects/personal/app/ansible/nginx.conf }
```

License
-------

BSD

Author Information
------------------

[bcooling.com.au](bcooling.com.au)
