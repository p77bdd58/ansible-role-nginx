# SEE: digitalocean.com/community/tutorials/how-to-optimize-nginx-configuration
# & https://github.com/h5bp/server-configs-nginx/blob/master/nginx.conf

user www-data;
pid /run/nginx.pid;

# Nginx processors (handles 1024 clients/second)
# ----------------

# Same value as cpu core(s). Run `nproc`
worker_processes 1;
# ulimit -n

events {
  worker_connections 1024;
}

http {

  # Hide nginx version information.
  server_tokens off;

  # Buffer sizes
  # ------------

  client_body_buffer_size 10K;
  client_header_buffer_size 1k;
  client_max_body_size 20m;
  large_client_header_buffers 2 1k;


  # timeout
  # -------

  client_body_timeout 12;
  client_header_timeout 12;
  # Amount seconds client has to send/recieve request/response
  keepalive_timeout 15;
  send_timeout 10;


  # compression
  # -----------

  gzip             on;
  gzip_comp_level  5;
  gzip_min_length  256;
  gzip_proxied     any;
  gzip_vary        on;
  gzip_types
      application/atom+xml
      application/javascript
      application/json
      application/ld+json
      application/manifest+json
      application/rdf+xml
      application/rss+xml
      application/schema+json
      application/vnd.geo+json
      application/vnd.ms-fontobject
      application/x-font-ttf
      application/x-javascript
      application/x-web-app-manifest+json
      application/xhtml+xml
      application/xml
      font/eot
      font/opentype
      image/bmp
      image/svg+xml
      image/vnd.microsoft.icon
      image/x-icon
      text/cache-manifest
      text/css
      text/javascript
      text/plain
      text/vcard
      text/vnd.rim.location.xloc
      text/vtt
      text/x-component
      text/x-cross-domain-policy
      text/xml;
    # text/html is always compressed by HttpGzipModule


  # housekeeping
  # ------------

  include /etc/nginx/mime.types;
	default_type application/octet-stream;

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

  # turn off access logs if using new relic
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

  # Speed up file transfers by using sendfile() to copy directly
  # between descriptors rather than using read()/write().
  sendfile on;

  # Tell Nginx not to send out partial frames; this increases throughput
  # since TCP frames are filled up before being sent out. (adds TCP_CORK)
  tcp_nopush on;

  include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;

  server {
    listen 80;
    server_name localhost;

    return 301 https://localhost$request_uri;
  }

  server {
      listen 443 ssl;
      server_name localhost;
      add_header Strict-Transport-Security "max-age=31536000; includeSubdomains";

      ssl_certificate     /etc/nginx/tls/server.crt;
      ssl_certificate_key /etc/nginx/tls/server.key;

      error_log  /var/log/nginx/error.log;
      access_log /var/log/nginx/access.log;

      location /assets {
        expires 1y;
        access_log off;
        add_header Cache-Control 'public';
        add_header Access-Control-Allow-Origin *;
        autoindex on; # TODO: remove this after testing
      }

      location / {
          proxy_pass http://localhost:3000;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection 'upgrade';
          proxy_set_header Host $host;
          proxy_cache_bypass $http_upgrade;
      }
  }

}
